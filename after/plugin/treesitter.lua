local configs = require("nvim-treesitter.configs")

configs.setup({
	ensure_installed = {
		"c",
		"go",
		"json",
		"lua",
		"python",
		"rust",
		"vim",
		"vimdoc",
		"yaml",
		"zig"
	},
	sync_install = false,
	highlight = { enable = true },
	indent = { enable = false },
})
