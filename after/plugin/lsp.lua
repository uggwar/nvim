local lsp = require('lsp-zero').preset({})

lsp.on_attach(function(_, bufnr)
	-- see :help lsp-zero-keybindings
	-- to learn the available actions
	lsp.default_keymaps({ buffer = bufnr })
	vim.keymap.set('i', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<cr>', { buffer = bufnr })
	vim.keymap.set('n', '<leader>f', '<cmd>lua vim.lsp.buf.format()<cr>', { buffer = bufnr })
end)

-- to learn how to use mason.nvim with lsp-zero
-- read this: https://github.com/VonHeikemen/lsp-zero.nvim/blob/v3.x/doc/md/guide/integrate-with-mason-nvim.md
require('mason').setup({})
require('mason-lspconfig').setup({
	ensure_installed = {},
	handlers = {
		lsp.default_setup,
	},
})

-- here you can setup the language servers
require('lspconfig').gopls.setup({})

require('lspconfig').zls.setup({})

require('lspconfig').ols.setup({})

require('lspconfig').pylsp.setup({})

require 'lspconfig'.lua_ls.setup(lsp.nvim_lua_ls())

require('lspconfig').nixd.setup({
	cmd = { "nixd" },
	settings = {
		nixd = {
			nixpkgs = {
				expr = "import <nixpkgs> { }",
			},
			formatting = {
				command = { "alejandra" },
			},
			options = {
				nixos = {
					expr = "(builtins.getFlake (\"/home/christer/Programming/nixos-config\")).nixosConfiguration.solitude.options",
				},
				home_manager = {
					expr = "(builtins.getFlake (\"/home/christer/Programming/nixos-config\")).homeConfiguration.solitude.options",
				},
			},
		},
	}
})
