require("toggleterm").setup{
	size = 100,
	open_mapping = [[<c-\>]],
	shade_filetype = {},
	direction = 'vertical',
}
