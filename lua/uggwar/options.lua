vim.g.mapleader = ' '

vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25

vim.o.smarttab = true
vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = false

vim.o.scrolloff = 3
vim.o.incsearch = true

vim.o.number = true
vim.o.relativenumber = true
vim.o.lazyredraw = true
