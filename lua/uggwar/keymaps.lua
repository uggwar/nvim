-- move the selection
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- keep cursor at 0 when joining lines
vim.keymap.set("n", "J", "mzJ`z")

-- keep cursor in center while jumping pages
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- keep search word in the middle
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- yank/paste but keep the original yank
vim.keymap.set("x", "<leader>p", "\"_dP")

-- yank into system clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- delete into the void
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

-- the worst place in the universe?
vim.keymap.set("n", "Q", "<nop>")

-- remove all search highlights
vim.keymap.set("n", "<leader>h", "<cmd>noh<cr>", {})

-- Traverse lines that are wrapped
vim.keymap.set({ "n", "o", "x" }, "j", "gj", {})
vim.keymap.set({ "n", "o", "x" }, "k", "gk", {})
vim.keymap.set({ "n", "o", "x" }, "0", "g0", {})
vim.keymap.set({ "n", "o", "x" }, "$", "g$", {})
vim.cmd([[set wrap]])

-- default targets in project justfile
vim.keymap.set("n", "<F9>", "<cmd>make run<cr>", {})
vim.keymap.set("n", "<F7>", "<cmd>make test<cr>", {})
vim.keymap.set("n", "<F8>", "<cmd>make<cr>", {})
vim.o.makeprg = 'just'
